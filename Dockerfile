FROM node:12.16.3

COPY package.json package.json
COPY package-lock.json package-lock.json

RUN npm install npm@latest
RUN npm ci
